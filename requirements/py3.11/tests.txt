#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.11/tests.txt requirements/tests.in
#
-e file:.#egg=idem
    # via
    #   -r requirements/tests.in
    #   pytest-idem
acct==8.5.2
    # via
    #   -r requirements/base.txt
    #   evbus-kafka
    #   evbus-pika
    #   idem
    #   pop-evbus
aio-pika==9.0.5
    # via evbus-pika
aiofiles==23.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   dict-toolbox
    #   idem
    #   pop-tree
aiokafka==0.8.0
    # via evbus-kafka
aiormq==6.7.4
    # via aio-pika
altgraph==0.17.3
    # via pyinstaller
async-timeout==4.0.2
    # via aiokafka
cffi==1.15.1
    # via cryptography
colorama==0.4.6
    # via
    #   -r requirements/base.txt
    #   -r requirements/tests.in
    #   idem
    #   rend
cryptography==40.0.1
    # via acct
dict-toolbox==4.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   pop-tree
    #   pytest-pop
    #   rend
evbus-kafka==4.0.1
    # via -r requirements/extra/kafka.txt
evbus-pika==3.0.1
    # via -r requirements/extra/rabbitmq.txt
idna==3.4
    # via yarl
importlib-metadata==6.1.0
    # via tiamat-pip
iniconfig==2.0.0
    # via pytest
jinja2==3.1.2
    # via
    #   -r requirements/base.txt
    #   idem
    #   rend
jmespath==1.0.1
    # via
    #   -r requirements/base.txt
    #   idem
kafka-python==2.0.2
    # via aiokafka
lazy-object-proxy==1.9.0
    # via pop
markupsafe==2.1.2
    # via jinja2
mock==5.0.1
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.5
    # via
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
multidict==6.0.4
    # via yarl
nest-asyncio==1.5.6
    # via
    #   pop-loop
    #   pytest-pop
packaging==23.0
    # via
    #   aiokafka
    #   pytest
pamqp==3.2.1
    # via aiormq
pluggy==1.0.0
    # via pytest
pop-config==12.0.1
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop
    #   pytest-pop
pop-evbus==6.2.3
    # via
    #   -r requirements/base.txt
    #   evbus-kafka
    #   evbus-pika
    #   idem
pop-loop==1.0.6
    # via
    #   -r requirements/base.txt
    #   idem
    #   pop
pop-serial==1.1.1
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-evbus
pop-tree==10.1.0
    # via
    #   -r requirements/base.txt
    #   idem
pop==23.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   evbus-kafka
    #   evbus-pika
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
pycparser==2.21
    # via cffi
pyinstaller-hooks-contrib==2023.1
    # via pyinstaller
pyinstaller==5.10.0
    # via
    #   -r requirements/package.in
    #   tiamat-pip
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.18.3
    # via pytest-pop
pytest-idem==3.1.0
    # via -r requirements/tests.in
pytest-pop==12.0.0
    # via
    #   -r requirements/tests.in
    #   pytest-idem
pytest==7.3.1
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
pyyaml==6.0.1
    # via
    #   -r requirements/base.txt
    #   acct
    #   dict-toolbox
    #   idem
    #   pop
    #   rend
rend==6.5.2
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
sniffio==1.3.0
    # via pop-loop
tiamat-pip==1.10.1
    # via -r requirements/package.in
toml==0.10.2
    # via
    #   -r requirements/base.txt
    #   idem
    #   rend
tqdm==4.65.0
    # via
    #   -r requirements/base.txt
    #   idem
uvloop==0.17.0 ; platform_system != "Windows"
    # via
    #   -r requirements/base.txt
    #   idem
wheel==0.40.0
    # via
    #   -r requirements/base.txt
    #   idem
yarl==1.8.2
    # via
    #   aio-pika
    #   aiormq
zipp==3.15.0
    # via importlib-metadata

# The following packages are considered to be unsafe in a requirements file:
# pip
# setuptools
