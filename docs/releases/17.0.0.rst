===========
Idem 17.0.0
===========

Idem 17.0.0 adds support for :ref:`argument binding<argument_binding>`.
