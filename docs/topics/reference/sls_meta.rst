============
SLS Metadata
============

Sometimes it may be desirable for metadata to be stored inside of an SLS file.
This can be useful for defining any additional data that an external system may
want to use that is not included inside of the Idem runtime.

SLS Level Metadata
==================

Add metadata to an SLS file is very simple, just make a top level key in the SLS
file called ``META``:

.. code-block:: sls

   META:
     foo: bar
     baz:
       - 1
       - True
       - "a string"

The ``META`` key is transferred into the idem run's running dict under the name ``meta``
and can be retrieved by anyone who has access to the run on the hub.

Found in ``hub.idem.RUNS[<run name>]["meta"]["SLS"]``

The metadata is stored relative to the SLS reference where the original metadata
was found.

ID Level Metadata
=================

Metadata can also be stored inside the ID Declarations, this allows for metadata
to be associated with an ID instead of just with the SLS file. Simply create a ``META``
key inside the ID Declaration:

.. code-block:: sls

   private_network:
     META:
       foo: bar
     cloud.vpc:
       - cidir: 10.0.0.0/16
