============
Progress Bar
============

Idem can show a progress bar for states.
The progress bar displays one tick mark for each state completed, regardless of success or failure.
Progress bars are printed to stderr and do not interfere with parsing program output.
The default behavior is to show the progress bar.

Configuration
=============

The config file supports the following options for progress bars:

.. code-block:: yaml

    # my_config.cfg

    idem:
      # Always show a progress bar
      progress: True
      # The progress plugin to use, currently only "tqdm" is available
      progress_plugin: tqdm
      # kwargs to pass to the progress bar plugin "create" function
      progress_options:
        mininterval: 0.5

CLI
===

The progress bar is enabled on the cli by default for the following cli commands:

.. code-block:: bash

    $ idem state my_state.sls

.. code-block:: bash

    $ idem describe my_resource

You can also explicitly disable the progress bar on the cli using the ``--no-progress-bar`` flag.

.. code-block:: bash

    $ idem state my_state.sls --no-progress-bar

Examples
========

To add a progress bar, create and call an SLS file such as progress.sls. Use the following examples as guidelines.

Basic Progress Bar
------------------

The following progress.sls file creates a simple progress bar:

.. code-block:: sls

    # progress.sls

    {% for i in range(100) %}
    sleep_{{ i }}:
      time.sleep:
        - duration: .1
    {% endfor %}

To show the progress bar, add the following command line options. Use serial runtime to clearly display incremental tick marks. Parallel runtime is instantaneous and won't show a slow progression of tick marks:

.. code-block:: bash

    $ idem state progress.sls --progress --runtime=serial

Progress bar output:

.. code-block:: text

    idem runtime: 0: 100%|¦¦¦¦¦¦¦¦¦¦| 100/100 [00:10<00:00,  9.63states/s]

Reconciliation
--------------

Idem reconciliation loops result in a progress bar for each loop as shown in the following example.

For demonstration purposes in the example, the first run is coded to fail in the ``nop`` state requisites. The failure then causes three reconciliation passes, each with its own progress bar:

.. code-block:: sls

    # progress.sls

    fail:
      test.present:
        - result: False

    nop:
      test.nop:
        - require:
            - fail

.. code-block:: bash

    $ idem state progress.sls --progress --runtime=serial

Progress bar output:

.. code-block:: text

    idem runtime: 0: 100%|¦¦¦¦¦¦¦¦¦¦| 2/2 [00:00<00:00, 959.79states/s]
    idem runtime: 1: 100%|¦¦¦¦¦¦¦¦¦¦| 1/1 [00:00<00:00, 266.02states/s]
    idem runtime: 2: 100%|¦¦¦¦¦¦¦¦¦¦| 1/1 [00:00<00:00, 264.14states/s]
    idem runtime: 3: 100%|¦¦¦¦¦¦¦¦¦¦| 1/1 [00:00<00:00, 274.86states/s]

Displaying Separate Progress Bars
---------------------------------

The ``unique`` requisite creates a new runtime for each use of the ``unique`` requisite, which results in a new progress bar for each state.

.. code-block:: sls

    # progress.sls

    {% for i in range(10) %}
    sleep_{{ i }}:
      time.sleep:
        - duration: .1
        - unique:
            - time.sleep
    {% endfor %}

.. code-block:: bash

    $ idem state progress.sls --progress --runtime=serial

Progress bar output:

.. code-block:: text

    idem runtime: 0:  10%|¦         | 1/10 [00:00<00:00,  9.80states/s]
    idem runtime: 0:  10%|¦         | 1/10 [00:00<00:00,  9.46states/s]

    idem runtime: 1:  11%|¦         | 1/9 [00:00<00:00,  9.56states/s]
    idem runtime: 1:  11%|¦         | 1/9 [00:00<00:00,  9.23states/s]
    idem runtime: 2:  12%|¦?        | 1/8 [00:00<00:00,  9.65states/s]
    idem runtime: 2:  12%|¦?        | 1/8 [00:00<00:00,  9.33states/s]

    idem runtime: 3:  14%|¦?        | 1/7 [00:00<00:00,  9.66states/s]
    idem runtime: 3:  14%|¦?        | 1/7 [00:00<00:00,  9.32states/s]
    idem runtime: 4:  17%|¦?        | 1/6 [00:00<00:00,  9.65states/s]
    idem runtime: 4:  17%|¦?        | 1/6 [00:00<00:00,  9.43states/s]

    idem runtime: 5:  20%|¦¦        | 1/5 [00:00<00:00,  9.70states/s]
    idem runtime: 5:  20%|¦¦        | 1/5 [00:00<00:00,  9.39states/s]
    idem runtime: 6:  25%|¦¦¦       | 1/4 [00:00<00:00,  9.63states/s]
    idem runtime: 6:  25%|¦¦¦       | 1/4 [00:00<00:00,  9.29states/s]

    idem runtime: 7:  33%|¦¦¦?      | 1/3 [00:00<00:00,  9.66states/s]
    idem runtime: 7:  33%|¦¦¦?      | 1/3 [00:00<00:00,  9.35states/s]
    idem runtime: 8:  50%|¦¦¦¦¦     | 1/2 [00:00<00:00,  9.65states/s]
    idem runtime: 8:  50%|¦¦¦¦¦     | 1/2 [00:00<00:00,  9.35states/s]

    idem runtime: 9: 100%|¦¦¦¦¦¦¦¦¦¦| 1/1 [00:00<00:00,  9.63states/s]
    idem runtime: 9: 100%|¦¦¦¦¦¦¦¦¦¦| 1/1 [00:00<00:00,  9.40states/s]

Progress Bars in PyCharm
========================

Some terminal consoles, such as the PyCharm *Run* window, do not flush or erase a line of text.

To correctly display progress bars in PyCharm, edit your Run configuration, and enable the *emulate terminal in output console* option.
