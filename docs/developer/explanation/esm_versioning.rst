==============
ESM Versioning
==============

Idem tracks the ESM version in the cache file metadata.

If the ESM cache version is *newer* than what idem supports, an error will be raised asking you to upgrade idem.
If the ESM cache version is *lower* than the current version in idem, an error will be raised asking you to upgrade the cache.
If the ESM cache version matches the version supported by the given version of idem, everything will run normally.

Upgrade ESM Cache
=================

To upgrade the ESM cache
Simply run your idem command with the ``--upgrade-esm`` flag,
or set it in your config file to always upgrade when necessary:

.. code-block:: yaml

    idem:
      upgrade_esm: True

ESM Version
===========

The latest ESM version supported by idem can be retrieved via this exec call:

.. code-block:: bash

    $ idem exec esm.version

.. note::

    If the command results in an attribute error,
    then you may be on an older version of idem that didn't track the ESM version
    or didn't have the ESM feature built yet.
    Upgrade to at least idem 20.4.0 for ESM version tracking.


ESM Upgrade Plugin
==================

An esm upgrade plugin tells idem how to upgrade the ESM cache from one explicit version to the next.

.. code-block:: python

    from typing import Any
    from typing import Dict
    from typing import Tuple


    def previous_version(hub) -> Tuple[int, int, int]:
        # TODO return a tuple of the previous version this plugin upgrades from
        return ..., 0, 0


    def apply(hub, esm_cache: Dict[str, Any]) -> Dict[str, Any]:
        """
        Upgrade from previous version to the next version
        """
        # Get the metadata from the esm file cache
        metadata = esm_cache[hub.idem.managed.ESM_METADATA_KEY]

        # Modify the esm_version in the metadata
        metadata["version"] = (..., 0, 0)

        # Apply the change to the esm file cache (This isn't a regular dictionary)
        esm_cache[hub.idem.managed.ESM_METADATA_KEY] = metadata

        return esm_cache
