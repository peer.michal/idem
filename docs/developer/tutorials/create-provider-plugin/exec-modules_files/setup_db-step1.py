"""Demo Idem Provider plugin for interacting with an sqlite database."""
import os
import sqlite3


async def setup_db(hub, ctx, db_path="~/idem-sqlite-demo.db"):
    """Create a new sqlite database.

    Args:

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.setup_db
    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    cur = con.cursor()

    # check if table already exists
    tables = cur.execute(
        """SELECT name FROM sqlite_master WHERE type='table'
        AND name='people'; """
    ).fetchall()
    if not tables:
        cur.execute(
            """CREATE TABLE people
                (resource_id integer primary key AUTOINCREMENT,
                name varchar(20) NOT NULL,
                age varchar(20) NOT NULL)"""
        )
        cur.execute(
            """
            INSERT INTO people VALUES
                (null, 'Sarah', 28),
                (null, 'David', 45)
                """
        )
        con.commit()

    vals = cur.execute("SELECT resource_id, name, age FROM people").fetchall()
    result["ret"] = list(vals)
    con.close()
    return result
