"""Demo Idem Provider plugin for interacting with an sqlite database."""
import os
import sqlite3

__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, db_path="~/idem-sqlite-demo.db"):
    """List all people database entries.

    Args:

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.list
    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    vals = cur.execute("SELECT resource_id, name, age FROM people").fetchall()
    result["ret"] = [list(row) for row in vals]
    con.close()
    return result
