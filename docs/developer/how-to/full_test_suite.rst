=======================
Running Full Test Suite
=======================

To run the test suite, follow these steps.

Setup
=====

Install docker https://docs.docker.com/engine/install/

Run a container with a kafka server for the event tests.

.. code-block:: bash

    $ docker run --rm -p 9092:9092  \
        -e KAFKA_ENABLE_KRAFT=yes \
        -e KAFKA_CFG_PROCESS_ROLES=broker,controller \
        -e KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER \
        -e KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093 \
        -e KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT \
        -e KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://127.0.0.1:9092 \
        -e KAFKA_BROKER_ID=1 \
        -e KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=1@127.0.0.1:9093 \
        -e ALLOW_PLAINTEXT_LISTENER=yes \
        bitnami/kafka:latest

Run a container with a rabbitmq server for the event tests.

.. code-block:: bash

    $ docker run -p 5672:5672 \
              --env RABBITMQ_HOSTS=localhost \
              --env RABBITMQ_PORT=5672 \
              --env RABBITMQ_USER=guest \
              --env RABBITMQ_PASS=guest \
              --env RABBITMQ_PROTOCOL=amqp \
              rabbitmq:management

Add the plaintext example credentials file to your environment variables.
Make sure to set an empty ACCT_KEY so that the credentials get read as plain text.

.. code-block:: bash

    $ export ACCT_FILE="$PWD/example/credentials.yml"
    $ export ACCT_KEY=""

Run Tests With Flexible Environment
===================================

Install pip 21, as later versions of pip cause issues with projects installed in editable mode.
Also install the test requirements, and the idem directory --in ediable mode.
Pytest allows a lot of flexibility and fine-tuned control over what is being tested.
The downside to pytest is that variations in the environment can cause difficult-to-debug behavior.

.. code-block:: bash

    $ pip install pip==21 -r requirements/tests.in
    $ pip install -e .

Run the tests with pytest!

.. code-block:: bash

    $ pytest tests

Running Tests With Static Environment
=====================================

The CI/CD pipelines on gitlab run the idem tests with nox.
Nox will install the idem requirements for you and run every test in the suite.
Nox allows us to have a static repeatable environment for testing.
The downside to Nox is that it takes a long time to run.

.. code-block:: bash

    $ pip install nox

Run the tests with nox!

.. code-block:: bash

    $ nox -p 3.10
