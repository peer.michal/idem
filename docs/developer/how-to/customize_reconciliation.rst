.. _customize_reconciliation:

==========================
Customizing Reconciliation
==========================

You can customize the Idem :ref:`reconciliation_loop` by:

* Adjusting the wait time
* Providing your own pending plugin

In addition, you can run the reconciliation loop as a batch command.

Reconciliation Wait Time
========================

Reconciliation wait time is the sleep time between loop iterations. Each state may define a separate wait time in seconds. The default is 3 seconds unless defined by a state.

At each iteration, wait time is recalculated for all pending states. The longest wait time is then used for loop iteration.

You can define a state wait time with one of the following algorithms:

* Static
* Random
* Exponential

Static
------

A fixed wait time for all iterations. For example:

.. code-block:: python

    __reconcile_wait__ = {"static": {"wait_in_seconds": 10}}

When you don't define any state wait time, a static 3 seconds is used by default.

Random
------

Minimum and maximum values in seconds, where wait time is a random number of seconds within that range, inclusive. For example:

.. code-block:: python

    __reconcile_wait__ = {"random": {"min_value": 1, "max_value": 10}}

In the example, a random number from 1 to 10 is generated before each reconciliation iteration.

Exponential
-----------

Wait time that increases for every reconciliation iteration. The exponential wait time is calculated based on the following formula:

``wait_in_seconds * (multiplier ^ run_count)``

Where ``run_count`` is the number of the iteration. The default value is 0. For example:

.. code-block:: python

    __reconcile_wait__ = {"exponential": {"wait_in_seconds": 2, "multiplier": 10}}

In the example, exponential wait times are: 2, 20, 200...

Pending Plugin
==============

The pending plugin determines whether a state is pending and in need of reconciliation. A returned value of *True* means the state is pending and needs to be reapplied.

To implement your own pending plugin, provide a method similar to the following:

.. code-block:: python

    def is_pending(hub, ret: dict, state: str = None, **pending_kwargs) -> bool:
        ...

The ``is_pending`` method returns *True* if more reconciliation is needed, otherwise *False*.

The ``is_pending`` method receives two arguments in ``pending_kwargs`` that apply to the state:

* ``reruns_count`` - total number of reruns

* ``reruns_wo_change_count`` - total number of reruns without changes, which means the same as ``result`` and ``changes``.

Add your pending plugin under ``idem/reconcile/pending`` and specify it in the ``idem state`` command with the ``--pending`` argument. For example:

.. code-block:: bash

    $ idem state file.sls --pending=aws

In Idem, the default implementation is defined in ``default.py`` and invokes a state's ``is_pending`` method if that method is implemented. A state's method has the same contract as above.

Any function can return ``rerun_data`` property as part of the return dictionary. Rerun data is any value that you want to maintain within the running context for all reconciliations. For example, you can use ``rerun_data`` to preserve metadata across reconciliations, such as a task identifier.


Batch Function
==============

You can also run the reconciliation loop in a batch command. For example:

.. code-block:: python

    hub.pop.Loop.run_until_complete(
        hub.idem.state.batch(
            states=states,
            name=name,
            runtime="serial",
            renderer="json",
            test=False,
            encrypted_profiles=encrypted_profiles,
            acct_key=acct_key,
            default_acct_profile="default",
            reconcile_plugin="basic",
            pending_plugin="default",
        )
    )
