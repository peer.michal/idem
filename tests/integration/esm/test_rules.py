import pathlib
import shutil

import msgpack
import pytest_idem.runner as runner

PASS_STATE = """
passing_state:
  test.present:
    - new_state:
       a: b
"""

CHANGED_STATE = """
passing_state:
  test.present:
    - new_state:
       a: c
"""

FAILED_STATE = """
passing_state:
  test.present:
    - result: false
    - new_state:
       a: c
"""

FAILED_STATE_FORCE_SAVE = """
passing_state:
  test.present:
    - result: false
    - force_save: true
    - new_state:
       a: c
"""

IGNORE_CHANGES_STATE_CREATE = """
passing_state:
  test.update:
    - resource_name: my-state
    - result: true
    - param_1: {a: b}
    - param_2:
       a: {b: c}
       b: c
    - ignore_changes:
      - param_2
"""

IGNORE_CHANGES_STATE_UPDATE = """
passing_state:
  test.update:
    - resource_name: my-state
    - result: true
    - param_1: {a: c}
    - param_2:
       a: {b: d}
       b: d
    - ignore_changes:
      - param_1
      - param_2
"""

IGNORE_CHANGES_STATE_UPDATE_2 = """
passing_state:
  test.update:
    - resource_name: my-state
    - result: true
    - param_1: {a: b}
    - param_2:
       a: {b: d}
       b: d
    - ignore_changes:
      - param_2:a:b
"""

IGNORE_CHANGES_STATE_UPDATE_RESOURCE_ID = """
passing_state:
  test.present_resource:
    - resource_name: my-state
    - result: true
    - enforce_state:
       param_1: a
       param_2: b
    - remote_state:
       param_1: c
       param_2: d
    - resource_id: "resource-id"
    - ignore_changes:
      - enforce_state:param_1
"""


def test_ctx_test(tmpdir, idem_cli):
    """
    When the `--test` flag is given on the cli, ESM should not be updated
    """
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    try:
        # Run the state to create an esm cache
        with runner.named_tempfile(suffix=".sls", delete=True) as fh:
            fh.write_text(PASS_STATE)

            idem_cli(
                "state",
                fh,
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--esm-plugin=local",
                "--run-name=test",
                check=True,
            )

            assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            metadata = data.pop("__esm_metadata__")
            assert metadata["version"]
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}

        # Change the state and run with the test flag,  nothing should change
        with runner.named_tempfile(suffix=".sls", delete=True) as fh:
            fh.write_text(CHANGED_STATE)

            idem_cli(
                "state",
                fh,
                "--test",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--esm-plugin=local",
                "--run-name=test",
                check=True,
            )

        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            metadata = data.pop("__esm_metadata__")
            assert metadata["version"]
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}
    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)
        assert not cache_dir.exists(), f"Could not remove cache dir: {cache_dir}"


def test_fail(tmpdir, idem_cli):
    """
    When a state fails during update, it should not update ESM
    """
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    # Run the state to create an esm cache
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(PASS_STATE)

        idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
        )

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}

    # Change the state so that it fails, nothing should change
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(FAILED_STATE)

        idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
        )

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}

    # Change the state with a failed state but with force_save to be True, this should result an esm update
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(FAILED_STATE_FORCE_SAVE)

        idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
        )

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "c"}}


def test_fail_on_create(tmpdir, idem_cli):
    """
    When a state fails during creation, it should update ESM if new_state is not empty
    """
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    # Run the state to create an esm cache
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(FAILED_STATE)

        idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
        )

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "c"}}


def test_refresh(tmpdir, idem_cli, hub):
    """
    Test esm when the "SUBPARSER" is refresh, it should update the cache
    """
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    ret = idem_cli(
        "refresh",
        "test_refresh",
        f"--cache-dir={cache_dir / 'cache'}",
        f"--root-dir={cache_dir}",
        "--esm-plugin=local",
        "--run-name=test",
        check=False,
    )

    assert "Changes were made by refresh" in ret.stderr

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        assert data == {"test_refresh_|-test_|-test_|-": {"changed": "value"}}


def test_ignore_changes_with_esm(tmpdir, idem_cli):
    """
    When the `ignore_changes` requisite is given in sls file, Idem should update those parameter values to None so that
    present() will ignore updating those parameters.
    """
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    # Run the state to create an esm cache
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(IGNORE_CHANGES_STATE_CREATE)

        idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
        )

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        assert data == {
            "test_|-passing_state_|-my-state_|-": {
                "param_1": {"a": "b"},
                "param_2": {"a": {"b": "c"}, "b": "c"},
            }
        }

    # ignore_changes contains param_1 and param_2, param_1 will be updated since it is a require parameter.
    # param_2 will be ignored on update.
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(IGNORE_CHANGES_STATE_UPDATE)

        idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
        )

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        assert data == {
            "test_|-passing_state_|-my-state_|-": {
                "param_1": {"a": "c"},
                "param_2": {"a": {"b": "c"}, "b": "c"},
            }
        }

        # ignore_changes contains a parameter path: param_1:a:b. Idem will go through the path and
        # assign {param_1: {a: {b: None}}}
        with runner.named_tempfile(suffix=".sls", delete=True) as fh2:
            fh2.write_text(IGNORE_CHANGES_STATE_UPDATE_2)

            idem_cli(
                "state",
                fh2,
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--esm-plugin=local",
                "--run-name=test",
                check=True,
            )

        assert esm_cache.exists()

        with esm_cache.open("rb") as fh3:
            data = msgpack.load(fh3)
            metadata = data.pop("__esm_metadata__")
            assert metadata["version"]
            assert data == {
                "test_|-passing_state_|-my-state_|-": {
                    "param_1": {"a": "b"},
                    "param_2": {"a": {"b": None}, "b": "d"},
                }
            }


def test_ignore_changes_with_resource_id(tmpdir, idem_cli):
    """
    When the `ignore_changes` requisite is given in sls file, Idem should update those parameter values to None
     if resource_id exists, so that present() will ignore updating those parameters.
    """
    cache_dir = pathlib.Path(tmpdir)
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(IGNORE_CHANGES_STATE_UPDATE_RESOURCE_ID)

        idem_cli(
            "state",
            fh,
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--esm-plugin=local",
            "--run-name=test",
            check=True,
        )

    assert esm_cache.exists()

    with esm_cache.open("rb") as fh:
        data = msgpack.load(fh)
        metadata = data.pop("__esm_metadata__")
        assert metadata["version"]
        # ignore_changes contains param_1. So param_1 will be ignored on update even when it's enforced value is
        # different from the remote value. param_2's remote value will be updated to the enforce value.
        assert data == {
            "test_|-passing_state_|-my-state_|-": {
                "param_1": "c",
                "param_2": "b",
            }
        }
