def test_outputters(hub, idem_cli):
    """
    Test all outputters to verify that they don't error out
    """

    for outputter in hub.output._loaded:
        ret = idem_cli("exec", "test.ping", f"--output={outputter}")
        assert ret.result, f"Exec could not parse output with '{outputter}' outputter"
