from idem.exec.init import ExecReturn


def test_object(hub):
    assert isinstance(ExecReturn(result=True), hub.exec.init.ExecReturn)
    assert isinstance(hub.exec.init.ExecReturn(result=True), ExecReturn)


def test_instance(hub):
    assert isinstance(hub.exec.init.ExecReturn(result=True), dict)


def test_subclass(hub):
    assert issubclass(hub.exec.init.ExecReturn, dict)
