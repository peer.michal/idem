import pytest


@pytest.mark.parametrize("resource_id_key", ["instance_id", "resource_id"])
def test_param_alias(resource_id_key, idem_cli):
    """
    Test for the ability of idem exec to respect param aliases
    """
    ret = idem_cli(
        "exec", "tests.alias.start", f"{resource_id_key}='{resource_id_key}'"
    )
    assert ret.retcode == 0, ret.stderr
    assert ret.json.ret == resource_id_key
