from pytest_idem.runner import run_sls


def test_dataclass_defaults():
    """
    Test populating default values for arguments that are defined as dataclasses
    """
    ret = run_sls(["dataclass"])
    tag = "test_dataclass_|-test_dataclass_|-test_dataclass_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"] is not None
    assert ret[tag]["new_state"].get("arg_flat") is not None
    # result is some SLS values some default values
    assert ret[tag]["new_state"].get("arg_flat") == {
        "ArgWithNone": "value",
        "ArgWithBoolTrue": True,
        "ArgWithBoolFalse": False,
        "ArgInt": 10,
        "arg_inner": {"InnerBoolTrue": True, "InnerBoolFalse": True, "InnerInt": 20},
    }
    assert ret[tag]["new_state"].get("arg_list") is not None
    assert len(ret[tag]["new_state"].get("arg_list")) == 2
    for elem in ret[tag]["new_state"].get("arg_list"):
        if elem.get("ArgInt") == 10:
            # All defaults
            assert elem == {
                "ArgWithBoolTrue": True,
                "ArgWithBoolFalse": False,
                "ArgInt": 10,
            }
        else:
            assert elem == {
                "ArgWithBoolTrue": True,
                "ArgWithBoolFalse": True,
                "ArgInt": 3,
            }

    # Test with values only in inner dataclass
    tag = "test_dataclass_|-test_dataclass_only_inner_|-test_dataclass_only_inner_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"] is not None
    assert ret[tag]["new_state"].get("arg_flat") is not None
    # result is some SLS values some default values
    assert ret[tag]["new_state"].get("arg_flat") == {
        "ArgWithBoolTrue": True,
        "ArgWithBoolFalse": False,
        "ArgInt": 10,
        "arg_inner": {"InnerBoolTrue": True, "InnerBoolFalse": False, "InnerInt": 3},
    }

    # Test with two inner dataclasses, they share the same inner field names
    # but different int default value (defaults only in inner)
    tag = "test_dataclass_|-test_dataclass_inner_tree_|-test_dataclass_inner_tree_|-present"
    assert ret[tag]["result"] is True
    assert ret[tag]["new_state"] is not None
    assert ret[tag]["new_state"].get("arg_dc_default_in_inner") is not None
    # result is some SLS values some default values
    assert ret[tag]["new_state"]["arg_dc_default_in_inner"].get("arg_inner") is not None
    assert (
        ret[tag]["new_state"]["arg_dc_default_in_inner"].get(
            "arg_inner_different_default"
        )
        is not None
    )
    assert ret[tag]["new_state"]["arg_dc_default_in_inner"].get("arg_inner") == {
        "InnerNone": "InnerInt_default_20",
        "InnerBoolTrue": True,
        "InnerBoolFalse": False,
        "InnerInt": 20,
    }
    assert ret[tag]["new_state"]["arg_dc_default_in_inner"].get(
        "arg_inner_different_default"
    ) == {
        "InnerNone": "InnerInt_default_40",
        "InnerBoolTrue": True,
        "InnerBoolFalse": False,
        "InnerInt": 40,
    }
