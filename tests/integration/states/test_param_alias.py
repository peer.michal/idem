import pytest
from pytest_idem.runner import run_yaml_block


@pytest.mark.parametrize("resource_id_key", ["resource_id", "id"])
def test_param_alias(resource_id_key):
    """
    Test for the ability of sls to use param alias
    """
    ESM = {}
    STATE = """
    param_alias:
      alias.present:
        - {resource_id_key}: success
    """
    ret = run_yaml_block(
        STATE.format(resource_id_key=resource_id_key), managed_state=ESM
    )

    result = next(iter(ret.values()))

    assert result["result"] is True
    # No matter which name the parameter has in SLS (the alias or original), it populated "resource_id"
    assert result["new_state"]["resource_id"] == "success"

    # Verify that it was stored in ESM with the official parameter name, not the alias
    assert ESM["alias_|-param_alias_|-param_alias_|-"] == {"resource_id": "success"}
