import json
import pathlib

import pytest
import yaml
from pytest_idem import runner

EXEC_RESOURCE = """
test:
  exec.stateful:
    - resource_id: my_resource_id
    - path: {path}
    - get_path: {get}
"""
ACCT_DATA = {"profiles": {"test": {"default": {}}}}
PARAMETRZIE_OPTS = dict(
    argnames="get_method",
    argvalues=["get", "", "name", "positional", "none"],
    ids=["get", "implied_get", "name", "positional", "none"],
)


@pytest.mark.parametrize(**PARAMETRZIE_OPTS)
def test_get(idem_cli, get_method):
    """
    Test that the various methods of performing a "get" work properly
    """
    get_path = f"test_resource.{get_method}" if get_method else ""
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(EXEC_RESOURCE.format(path="test_resource.action", get=get_path))
        ret = idem_cli(
            "state",
            fh,
            "--esm-plugin=null",
            "--run-name=test",
            "--no-progress",
            check=True,
            acct_data=ACCT_DATA,
        )

    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stdout
    state_result = next(iter(ret.json.values()))
    assert state_result["result"] is True
    assert state_result["esm_tag"] == "test_resource_|-test_|-test_|-"
    assert state_result["old_state"]["inc"] == 0
    assert state_result["new_state"]["inc"] == 1
    assert state_result["changes"] == {"old": {"inc": 0}, "new": {"inc": 1}}


@pytest.mark.parametrize(**PARAMETRZIE_OPTS)
def test_get_only_with_resource_id(idem_cli, get_method):
    """
    Test that the various methods of performing a "get" work properly
    """
    get_path = f"test_resource.{get_method}" if get_method else ""
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(EXEC_RESOURCE.format(path="test_resource.action", get=get_path))
        ret = idem_cli(
            "state",
            fh,
            "--esm-plugin=null",
            "--run-name=test",
            "--no-progress",
            "--get-resource-only-with-resource-id",
            check=True,
            acct_data=ACCT_DATA,
        )

    assert ret.result is True, ret.stderr or ret.stdout
    assert ret.json, ret.stdout
    state_result = next(iter(ret.json.values()))
    if get_method in ("get", ""):
        assert state_result["result"] is True
        assert state_result["esm_tag"] == "test_resource_|-test_|-test_|-"
        assert state_result["old_state"]["inc"] == 0
        assert state_result["new_state"]["inc"] == 1
        assert state_result["changes"] == {"old": {"inc": 0}, "new": {"inc": 1}}
    else:
        assert state_result["result"] is False


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRZIE_OPTS)
async def test_esm(hub, tmpdir, get_method):
    get_path = f"test_resource.{get_method}" if get_method else ""
    cache_dir = pathlib.Path(tmpdir)
    name = f"test-exec-stateful-{get_method}"
    STATE = {
        "test": yaml.safe_load(
            EXEC_RESOURCE.format(path="test_resource.action", get=get_path)
        )
    }

    context_manager = hub.idem.managed.context(
        run_name=name,
        cache_dir=cache_dir,
        esm_plugin="local",
        esm_profile=None,
        acct_file="",
        acct_key="",
        serial_plugin="msgpack",
        upgrade_esm=False,
    )
    async with context_manager as state:
        await hub.idem.state.apply(
            name=name,
            sls_sources=[f"json://{json.dumps(STATE)}"],
            render="yaml",
            runtime="serial",
            subs=["states"],
            cache_dir=cache_dir,
            sls=["test"],
            target=None,
            test=False,
            invert_state=False,
            acct_data=ACCT_DATA,
            managed_state=state,
            param_sources=[],
            params=[],
        )
        # Verify that changes were made to ESM after running the exec call
        assert state["test_resource_|-test_|-test_|-"]["inc"] == 1
