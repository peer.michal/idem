from pytest_idem.runner import run_sls


def test_arg_bind_in_name():
    """
    Test that you can do arg_binding
    """
    ret = run_sls(["arg_bind.arg_bind_in_name"])
    assert len(ret) == 3
    assert "test_|-state_A_|-state_A_|-present" in ret
    assert "test_|-state_B_|-state_a_name_|-present" in ret
    assert "test_|-state_C_|-state_C_|-present" in ret
