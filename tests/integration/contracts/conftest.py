import pytest
from pytest_idem.runner import tpath_hub


@pytest.fixture(scope="function", name="hub")
def contracts_tpath_hub():
    with tpath_hub() as hub:
        yield hub
