{% set organization_unit_1 = 'CloudGuardrails' %}
{% set tag_key_value = {"product":"vra,vrli" , "product2": "be,dev"} %}
{% set org_ou_tag = params.get('tag_key_value', tag_key_value) %}
{{organization_unit_1}}:
  test.state.present:
  - parent_id: 'o-test-org-1'
  - org_unit_name: {{organization_unit_1}}
  - tags: {{org_ou_tag}}
