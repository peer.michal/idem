State {{ params.get('state').get('name') }} Present:
  test.present:
    - name: {{ params.get('state').get('name') }}
    - result: True
    - new_state:
        location: {{ params['locations'][0] }}
        backup_location: {{ params['locations'][1] }}
        empty: {{ params.get('empty') }}
        empty_str: {{ params.get('empty') }}_str
        include_1: {{ params.get('include_one') }}
        include_2: {{ params.get('include_two') }}
        non_exists_default_val: {{ params.get('non_exists', 'default_val') }}
        eu_locations: {{ params['nested_list']['eu_locations'] }}
