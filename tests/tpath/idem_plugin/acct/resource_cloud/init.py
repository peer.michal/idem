GATHER_CALLED = False


async def gather(hub, profiles):
    hub.acct.resource_cloud.init.GATHER_CALLED = True

    if not profiles:
        new_profiles = {"default": {"new_profile": "value"}}

        return new_profiles
    else:
        return profiles
