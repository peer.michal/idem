# This module implements a contract that doesn't exist for exec modules
# This causes obscure hard-to-trace bugs in describe
__contracts__ = ["resource"]
