__contracts__ = ["returns", "soft_fail"]

import dict_tools.data


def get(hub, ctx):
    return {"result": True, "comment": None, "ret": ctx}


def more(hub, ctx, *args, **kwargs):
    """
    Return the ctx and all parameters passed to this state.
    """
    return {
        "result": True,
        "comment": None,
        "ret": {"args": args, "kwargs": kwargs, "ctx": ctx},
    }


def assert_ctx_type(hub, ctx, *args, **kwargs):
    """
    Assert that the input ctx is a NamespaceDict.
    It is automatically converted to a NamespaceDict on return.
    """
    assert isinstance(ctx, dict_tools.data.NamespaceDict)
    return {"result": True, "comment": None, "ret": ctx}
