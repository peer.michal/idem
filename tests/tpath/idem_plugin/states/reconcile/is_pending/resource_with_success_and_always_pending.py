"""Expectation: If is_pending present and result is "True", then we delegate the decision to is_pending"""
__contracts__ = ["resource"]
__reconcile_wait__ = {"static": {"wait_in_seconds": 1}}


def present(hub, ctx, name: str, number_of_reruns: int = 0):
    """
    Returns failure in state execution
    """
    execution_count = (
        ctx.get("rerun_data", {}).get("execution_count", 0)
        if ctx.get("rerun_data")
        else 0
    )
    execution_count += 1

    return {
        "name": name,
        "result": True,
        "comment": "test",
        "rerun_data": {"execution_count": execution_count},
        "old_state": {},
        "new_state": {"number_of_reruns": number_of_reruns},
    }


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"resource_with_success_and_always_pending.present": []}}


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs) -> bool:
    # This stops after configured --max-pending-runs
    return True
