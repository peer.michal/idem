"""Expectation: If changes are {}, even if new_state and old_state differ,
reruns_wo_change_count should increase
and reconciliation should stop after 3 runs"""
__contracts__ = ["resource"]
__reconcile_wait__ = {"static": {"wait_in_seconds": 1}}


def present(hub, ctx, name: str, relevant_prop: str):
    """
    old_state and new_state differ in an irrelevant property, changes are explicitly empty
    """
    execution_count = (
        ctx.get("rerun_data", {}).get("execution_count", 0)
        if ctx.get("rerun_data")
        else 0
    )
    execution_count += 1

    return {
        "name": name,
        "result": False,
        "comment": "test",
        "rerun_data": {"execution_count": execution_count},
        "old_state": {"irrelevant_prop": -1, "relevant_prop": relevant_prop},
        # new_state changes with each execution -> calculated "changes" would change
        "new_state": {
            "irrelevant_prop": execution_count,
            "relevant_prop": relevant_prop,
        },
        "changes": {},
    }


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"resource_with_pending_and_overridden_empty_changes.present": []}}


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs) -> bool:
    # This stops after 1 + 2 reruns
    return pending_kwargs and pending_kwargs.get("reruns_wo_change_count", 0) < 2
