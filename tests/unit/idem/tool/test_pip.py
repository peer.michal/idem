import unittest.mock as mock


def test_freeze(hub):
    freeze = hub.tool.pip.freeze()
    for module, item in freeze.items():
        assert "version" in item
        assert "editable" in item


def test_version(hub):
    assert hub.tool.pip.version()


def test_cli(hub, mock_hub):
    hub.pop.config.load = mock_hub.pop.config.load
    hub.tool.pip.version = mock_hub.tool.pip.version
    hub.tool.pip.freeze = mock_hub.tool.pip.freeze
    hub.log.warning = mock_hub.log.warning
    hub.idem.init.cli_apply = mock_hub.idem.init.cli_apply

    mock_hub.tool.pip.version.return_value = (22, 0, 0)
    mock_hub.tool.pip.freeze.return_value = {
        "idem": {"version": "1.2.3", "editable": True}
    }

    with mock.patch("sys.exit"):
        hub.idem.init.cli()

    mock_hub.log.warning.assert_called_with(
        "install pip==21 or some modules may not load properly"
    )
